export default function () {
    const butt = document.querySelectorAll('.info-titles');
    const images = document.querySelectorAll('.image-wrap');
    butt.forEach(elem => {
        // eslint-disable-next-line no-param-reassign
        elem.onclick = () => {
            const close = elem.querySelector('.togle-icon');
            elem.nextElementSibling.classList.toggle('show__this');
            close.classList.toggle('rotate-open');
        };
    });
    images.forEach(item => {
        // eslint-disable-next-line no-param-reassign
        item.onclick = (e) => {
            const div = document.createElement('div');
            const father = document.querySelector('#producrtSlide');
            const modal = document.querySelector('#hideBlock');
            const closed = document.querySelector('.close-slide span');
            div.classList.add('slide-init');
            let slides = '';
            slides += `<div class='slid-modal' style='background-image: ${e.target.style.backgroundImage}' ></div>`;
            images.forEach(el => {
                if (e.target.style.backgroundImage !== el.style.backgroundImage) {
                    slides += `<div class='slid-modal' style='background-image: ${el.style.backgroundImage}' ></div>`;
                }
            });
            div.innerHTML = slides;
            father.appendChild(div);
            modal.style.display = 'block';
            $('.slide-init').slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
            });
            closed.onclick = () => {
                modal.style.display = 'none';
                father.innerHTML = '';
            };
        };
    });
    const scrollDown = () => {
        const arrowDown = document.querySelector('#scrollDown');
        arrowDown.onclick = () => {
            window.scrollTo({ top: 480, behavior: 'smooth' });
        };
    };
    if (window.location.pathname === '/') {
        scrollDown();
    }
}
